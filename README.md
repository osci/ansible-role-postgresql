# Ansible role for PostgreSQL installation

## Introduction

[PostgreSQL](https://www.postgresql.org/) is an SQL database.

This role installs the server and initializes a default cluster.
On Debian systems this role only uses the 'main' default cluster.

This role manage the configuration file with defaults which should be
suitable for most users. If you need to customize, then use the
`conf.d` subdirectory to drop configuration file bits (PostgreSQL >=9.3
required), The firewall configuration is left to your care.

## Usage

To read parameters documentation, use this command:

```
ansible-doc -t role postgresql

```
